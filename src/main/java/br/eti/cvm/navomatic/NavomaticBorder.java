package br.eti.cvm.navomatic;

import org.apache.wicket.markup.html.border.Border;

public class NavomaticBorder extends Border {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NavomaticBorder(final String componentName) {
		super(componentName);
		addToBorder(new MyBorder("navigationBorder"));
		addToBorder(new MyBorder("bodyBorder"));
	}
}